package daoConnectic;

import com.programming.stock.Main;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoClass {


    Main main = new Main();

    public Connection selectData(String url, String querySql){

        try{

            File f = new File("dq_stock2.db");
            String absPath = f.getAbsolutePath();
            System.out.println(absPath);
            //  db parameters
            // absPath

            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }


            /*jdbc:sqlite:/Users/easycash/GitLabRepositories/stock-action/src/datasqlite/dq_stock2.db*/
            Connection connection =
                    DriverManager.getConnection(url);
            System.out.println("ok_sql_connects");

            return connection;

        } catch (SQLException e){

            System.out.println("error sql");
            e.printStackTrace();
            System.out.println(e.getMessage());

        }

        return null;

    }

}
