package defines.structures;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ElementOfAction
{

    Date dateTrading=null;
    String libelle;
    Double montantUnit;
    Double montantGlobal;
    Integer quantityUnit;
    Double sumOfMontantByAction;


    public Date getDateTrading() {
        return dateTrading;
    }

    public String getLibelle() {
        return libelle;
    }

    public Double getMontantUnit() {
        return montantUnit;
    }

    public Double getMontantGlobal() {
        return montantGlobal;
    }

    public Integer getQuantityUnit() {
        return quantityUnit;
    }


    public Double getSumOfMontantByAction() {
        return sumOfMontantByAction;
    }

    /*setters*/

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setDateTrading(Date dateTrading) {
        this.dateTrading = dateTrading;
    }

    public void setMontantUnit(Double montantUnit) {
        this.montantUnit = montantUnit;
    }

    public void setMontantGlobal(Double montantGlobal) {
        this.montantGlobal = montantGlobal;
    }

    public void setQuantityUnit(Integer quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public void setSumOfMontantByAction(Double sumOfMontantByAction) {
        this.sumOfMontantByAction = sumOfMontantByAction;
    }
}
