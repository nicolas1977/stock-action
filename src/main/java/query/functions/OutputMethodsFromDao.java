package query.functions;

import defines.structures.ElementOfAction;
import inter.user.InterfaceDemandsUsers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class OutputMethodsFromDao {

    /*TODO HERE  FUNCTION FOR ARRAYLIST OUTPUT*/

    public ArrayList<ElementOfAction> outputValuesFromQuery(Connection connectDummy,ArrayList<ElementOfAction> arrayListOutputSql){

        InterfaceDemandsUsers interfaceDemandsUsers = new InterfaceDemandsUsers();
        // PreparedStatement stm = null;


        try (Statement stm = connectDummy.createStatement();

             ResultSet rs = stm.executeQuery("SELECT * FROM stock");

        ){

            while(rs.next()) {

                ElementOfAction elementOfAction = new ElementOfAction();

                elementOfAction.setLibelle(rs.getString("lib"));
                elementOfAction.setMontantUnit(rs.getDouble("montant"));
                elementOfAction.setQuantityUnit(rs.getInt("quant"));

                /*
                arrayListActions.set(i, elementOfAction.setLibelle(libelle ));
                arrayListActions.set(i, elementOfAction.setQuantityUnit(quantity));
                arrayListActions.set(i, elementOfAction.setMontantUnit(montant));
                */

                arrayListOutputSql.add(elementOfAction);

                /*

                    NOTE : MAYBE THE FIRST ELEMENT IS RS.NEXT(); IS 0 , SO WHEN
                    THE RS.NEXT IS IN THE END OF ITERATION, THE FIRST RECORDS IS REPEATED TWO TIMES

                */

            }

            interfaceDemandsUsers.printArray(arrayListOutputSql);

        }catch(Exception e) {

            e.printStackTrace();

        }

        return arrayListOutputSql;

    }



}
