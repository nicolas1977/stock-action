package inter.user;

import defines.structures.ElementOfAction;
import query.functions.InputMethodsToDao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

public class SequentialDirectorUser {


    InterfaceDemandsUsers interfaceDemandsUsers = new InterfaceDemandsUsers();
    InputMethodsToDao inputMethodsToDao = new InputMethodsToDao();
    final Scanner keyboard = new Scanner( System.in );

    public void userData(Connection connection, ArrayList<ElementOfAction> arrayInputListSql,ArrayList<ElementOfAction> arrayListOutputSql){

        SequentialDirectorUser sequentialDirectorUser = new SequentialDirectorUser();

        System.out.println("WELCOME TO APPLICATION");

        System.out.println("WHAT YOU WANT MAKE ? 1 - ENTERED DATA, 2 - EXTRACTION, 3 - UPDATE OR 4 - EXIT ?");

        String inputQuestion = keyboard.nextLine();

        if ("1".equals(inputQuestion)) {

            sequentialDirectorUser.sequentialEnter(connection,arrayInputListSql);

        } else if ("2".equals(inputQuestion)){

            sequentialDirectorUser.sequentialOutput(connection,arrayListOutputSql);

        } else if ("3".equals(inputQuestion)) {

            // TODO

        } else if ("4".equals(inputQuestion)) {

            System.out.println("THANKS FOR USING THE PROGRAM");
            System.exit(0);

        }

        System.out.println("YOU ARE SELECTED : " + inputQuestion);

    }


    public void sequentialEnter(Connection connection, ArrayList<ElementOfAction> arrayListInputSql) {

        String inputFinish;

        do {

            System.out.println("----------------------------------------");
            System.out.println("ENTER THE NEXT RECORD :");
            System.out.println("----------------------------------------");

            /*userQuestioningInterface() return ElementOfAction*/
            /*interfaceDemandsUsers.userQuestioningInterface();*/

            inputMethodsToDao.enteredValuesInsertQuery(arrayListInputSql,
                    interfaceDemandsUsers.userQuestioningInterface());
/*


            inputMethodsToDao.enteredValuesInsertQuery(arrayListInputSql,
                    interfaceDemandsUsers.getLibelle(),
                    interfaceDemandsUsers.getMontant(),
                    interfaceDemandsUsers.getQuantity())
*/

            System.out.println("DO YOU FINISH TO ENTERED ?");
            inputFinish = keyboard.nextLine();

            if (inputFinish == "Y") {
                System.out.println("OK FINISH LAST ENTERED LINE");
            } else if (inputFinish == "N") {
                /*NOTHING*/
            }


        } while (inputFinish == "N");


    }


    public void sequentialOutput(Connection connection, ArrayList<ElementOfAction> arrayListOutputSql){


        // TODO


    }




}
