package converters.formats;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class DateAnalyses implements convertersFormatsI {

    /* instance of format*/
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public String returnStringByDate(Date dateTrading) {

        /* date to String*/
        String stringDate = dateFormat.format(dateTrading);

        return stringDate;

    }

    @Override
    public Date returnDateByString(String dateString) {

        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
            // e.printStackTrace();
        }

    }

    @Override
    public Integer returnIntegerByString(String dateString) {

        /* int parNum = Integer.valueOf(myString);*/

        int valueInt = Integer.parseInt(dateString);

        return valueInt;

    }

    @Override
    public String returnStringByInteger(Integer dateInteger) {

        /*Using the Integer.toString() method.
        Using the String.valueOf() method.
        Using the String.format() method.
        Using the DecimalFormat class.*/

        String valueStr = Integer.toString(dateInteger);

        return valueStr;

    }


    // public arrayList insertElementArrayList (




}