package converters.formats;

import java.util.Date;

public interface convertersFormatsI {

    Date returnDateByString(String dateString);
    String returnStringByDate(Date dateTrading);
    String returnStringByInteger(Integer dataString);
    Integer returnIntegerByString(String dataInteger);


}
